# CSSLP

Certified Secure Software Lifecycle Profesional is the credential assuring software professionals as capable of implementing security within systems development lifecycle. It is intended for Programmers, Software Architects, Project Managers, Software Auditors and in general those interested in knowing the techinques and methodologies of Secure Software Development.

## (ISC)2

The <span style="color: #cc0">International Information System Security Certification Consortium</span> is the most prestiged <span style="color: #c00">information system security</span> <span style="color: #55f">certification organization</span> with more than 80,000 members in 135 countries.

## About CSSLP

<span style="color: #cc0">Vulnerable software</span> is one of the fundamental causes for many <span style="color: #c00">security incidents</span>, given the <span style="color: #55f">ever more complex nature of software</span>, said vulnerabilities are not a problem that can be solved in the short term.

<span style="color: #0c0">Reducing the amount and graveness of vulnerabilities is possible and useful</span> in software projects. CSSLP is <span style="color: #cc0">not only centered on the aspects regarding <span style="color: #0c0">secure software development</span> <span style="color: #c00">but also those of the network and server</span> where software will be executed</span>.

## Test tips

### Know your basic IT Security Terminology and how to implement these as requirements

1. CIA Triad: Confidentiality, Integrity, Availability. Its main goal is to help enable their organizations structure their security posture appropriately. It is a well known staple in the world of IT Security, one of the most popular security frameworks in IT Security.

    - Confidentiality: the information cannot be understood by anyone for whom it was NOT intended.
    - Integrity: the information cannot be altered in storage or transit between sender and intended receiver without the alteration being detected.
    - Availability: the information is available to the user when requested.

1. Authentication vs Authorization

    Authentication: your identity, who you are.
    Authorization: your privileges/permissions, what you're allowed to do.

    e.g. You enter building, and your identity is verified by checking your credentials so you are allowed in (authentication), however, from 12 floors in the building you are only allowed at floors 11, 12 and the rooftop, from those floors, you are only allowed at offices 51, 53 and 55 (authorization).

1. Accountability

1. Repudiation

    Being able to ensure that we can provide some kind of proof to authenticate the user. Typically through a digital certificate.

### Know how to address PKI certificates

- Certificates (e.g. X.509)
- Private Key and Public Key use
- Digital Signatures

### Failure Handling

1. Fail Safe: Minimize harm to systems/personnel when a system fails.

1. Fail secure: block access to the system when it's not in a consistent state. e.g. a bank vault is locked by default in case of power failure

1. Fail open: Allows for ways to fix the problem. e.g. CTRL+ALT+DEL to access task manager when windows gets stuck

1. Exception management: a fail safe mechanism.

### Terminology aroung single point of failures

1. System resilience

1. Fault Tolerance

1. Availability

1. Load balancing

1. Autoscaling

1. SPOF (Single Point of Failure) - Handle which ways? IETV (Identify, Evaluate, Test, Validate)

### Principles of Secure Design

Principles of secure design underlie all security-related mechanisms and the proper design needs to be applied
    
When applying designs consider that `simplicity means`:

- KISS - keep it simple, stupid.
- Reduce possible inconsistencies
- Should be easier to understand restrictions
- Minimize access/interactions (requests)
- Reduce or eliminate communications

### Identify Security Requirements

- Intellectual property:
    - Intangible assets produced by a party.
    - <s>Includes software and data as well</s>.
    - Two types:
        1. Industrial property (patents, designs, diagrams, software).
        1. Trademark and copyrights (logos, books, trade markings, etc. (literary and artistic works)).

- Patents
    - A patent is a property right granted by a government allowing the holder to exclude other from making, using, offering for salem or selling the protected invention.

- Copyrights
    - Protects original works of authorship that are fixed in a tangible format (literary, dramatic, musical and artistic works such as poetry, novels, movies, songs, computer software and architecture, etc.).

- Trademarks
    - Used to help consumers tell the difference between the products of one company versus those of another company.
    - Gives legal protection to a word, phrase or symbol that is used in commerce.

- Trade Secrets
    - Trade secrets are a form of intellectual property.
    - Accoding to most US states, a trade secret may consist of any formula, pattern, physical device, idea, process or compilation of information that both:
    1. Provides the owner of the information with a competitive advantage in the marketplace.
    1. Is treated in a way that can reasonably be expected to prevent the public or competitors from learning about it, absent improper acquisition or theft.

- Warranty
    - A term of a contract.
    - Binds seller to specific guaratees.
    - Consumer warranties are either full or limited warranties.

- Privacy Issues
    - Privacy can be defined as an individual condition of life characterized by exclusion from publicity.
    - Technology and privacy generally conflict in many ways.

### Policy decomposition

- Policies are directive or guidance.
- Policies are issued by high level direction.
- Policies establish baselines and rules.
- Policies need to be identified as a policy.
- Policies need to be documented.
- Policies need to be clearly stated.
- Policies need to be enforced.
- Policies should be updated.
- A solid policy should define specific behaviors or references.
- Standards.
- Baselines.
- Best practices.
- Workflows.

Appropriate policies may provide for...
- A detailed framework for security.
- Address management concerns.
- Provide evidence of compliance.

### Security Guidelines

- Security Guidelines should be defined and implemented by policy.
- Security Guidelines should be benchmarked.
- Security Guidelines should be templated.
